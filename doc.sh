#!/bin/bash
set -eou pipefail

# cargo +nightly rustdoc --lib  --target-dir target-html -- -Zunstable-options -w html
cargo +nightly rustdoc --lib  --target-dir target-json -- -Zunstable-options -w json
# cargo +nightly rustdoc --lib  --target-dir target-html-private -- -Zunstable-options -w html  --document-private-items 
cargo +nightly rustdoc --lib  --target-dir target-json-private -- -Zunstable-options -w json --document-private-items 
