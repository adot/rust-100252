#!/bin/bash
set -eoxu pipefail
rm target-json/doc/clap.json target-json-private/doc/clap.json 
./doc.sh
./check.py target-json/doc/clap.json 
./check.py target-json-private/doc/clap.json
~/go/bin/oj -p 200.200 target-json/doc/clap.json > clap.json 
~/go/bin/oj -p 200.200 target-json-private/doc/clap.json > clap-private.json 