#!/usr/bin/env python3

import json
import sys
from pprint import pp

[_, file] = sys.argv

with open(file) as f:
    rdj = json.load(f)

index = rdj["index"]

for id, item in index.items():
    if item["kind"]=="trait" and item["name"]=="UnwindSafe":
        unwind_safe = id
        print("Found UnwindSafe trait at", id)
    if item["kind"]=="struct" and item["name"] == "ArgMatches":
        arg_matches_id = id
        print("Found ArgMatches struct at", id)
        

arg_matches_impls = index[arg_matches_id]["inner"]["impls"]

print("Found", len(arg_matches_impls), "impls for ArgMatches")

if len(arg_matches_impls) == 0:
    print("NO IMPLS?")

for impl in arg_matches_impls:
    item = index[impl]
    trait = item["inner"]["trait"]
    if trait is not None and trait["inner"]["id"] == unwind_safe:
        print("Found impl for UnwindSafe at", impl)